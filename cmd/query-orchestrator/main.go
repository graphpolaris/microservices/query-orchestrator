/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"query-orchestrator/internal/drivers/webdriver"
	"query-orchestrator/internal/usecases/produce"
	"query-orchestrator/internal/usecases/retrievequery"

	"git.science.uu.nl/graphpolaris/go-common/microservice"
)

/*
This is the main method, it executes the code for this service
*/
func main() {
	// Set up logging
	microservice.SetupLogging()

	// Create broker driver
	brokerDriver := microservice.ConnectRabbit()

	// Create keyvaluestore to get the queue with which clients are connected (websockets)
	redisService := microservice.ConnectRedis()
	retrieveService := retrievequery.NewService(redisService)

	produceService := produce.NewService(brokerDriver, redisService)
	produceService.Start()

	// Create webdriver
	webDriver := webdriver.CreateListener(produceService, retrieveService)
	webDriver.Start()

	select {}
}
