/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package errorhandler

import (
	"fmt"
)

/*
LogError logs an error that is not nil
	err: error, the error that occurred
	msg: string, the message describing the error
*/
func LogError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %v", msg, err)
	}
}

/*
FailWithError panics if the error is not nil
	err: error, the error that occurred
	msg: string, the message describing the error
*/
func FailWithError(err error, msg string) {
	if err != nil {
		fmt.Printf("%s: %v", msg, err)
		panic(err)
	}
}
