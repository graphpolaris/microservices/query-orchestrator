[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

<div align='center'>
<img src="https://git.science.uu.nl/GraphPolaris/frontend/-/raw/develop/src/presentation/view/navbar/logogp.png" align="center" width="150" alt="Project icon">
</div>

## Query Orchestrator Service

This service routes incoming query requests to [query executor](https://git.science.uu.nl/GraphPolaris/microservices-backbone/query-service) services with the required query language and database code running. It also allows for the retrieval of cached query results, which can be requested from the frontend. In the future we envision this service as a more advanced load balancer, as RabbitMQ only has round robin load balancing, which does not work if requests take arbitrary amounts of time.

## Development

For quick development, add to the go.mod file these two replace lines, which will fetch the respective dependency code from the local repo instead of from git:
DO NOT PUSH THEM!

```sh
replace git.science.uu.nl/graphpolaris/broker => ../../dependencies/broker

replace git.science.uu.nl/graphpolaris/keyvaluestore => ../../dependencies/keyvaluestore

```

### Dev Dependencies

Here are the developer dependencies. Coding can be done without these tools, but they sure make your life easier.

- Make
- Docker

### Dependencies

This service depends on quite some other services, as it is essentially just a shell for the query conversion and execution packages.

- RabbitMQ
- Minio
- Redis
- User management service (for database type retrieval -> for routing)

### Environment Variables

Here are the environment variables used in this service:

- `RABBIT_HOST` address of the RabbitMQ instance (string)
- `RABBIT_PORT` RabbitMQ port (int)
- `RABBIT_USER` RabbitMQ username (string)
- `RABBIT_PASSWORD` RabbitMQ password (string)
- `REDIS_ADDRESS` Redis address (string)
- `REDIS_PASSWORD` Redis password (string)
- `LOG_MESSAGES` Whether to log messages (bool)
- `JWT_SECRET` The secret used to sign and verify JWT's (string)

### Building the Service

To compile the service into a single binary, the following commands can be used. Go can build for many platforms, so we have three build commands, for the three main platforms.

```
make linux
make windows
make macos
```

To build the service and push it to the Docker registry the `make docker` command can be used.

### Testing and Coverage

To test the `make test` command can be used. It will run every test in the repo. To get the code coverage the command `make coverage` can be used. This command will run all tests and display total coverage over all files. It will also generate an html file which displays exactly which lines have been covered.

### Kubernetes

All the Kubernetes config files live in the `/deployments` directory. This service has a deployment which sets all required environment variables, some of which from Kubernetes secrets. The service also has a Kubernetes service config to allow the Ingress to route traffic to this service, as well as a HPA to scale the service if load increases.

### Service Flow

<div align='center'>
<img src="images/query_orchestrator_service.png" align="center" width="75%" alt="Query orchestrator service flow">
</div>

### Service Input

Here is the input message format.

```javascript
{
	"databaseName": "test_db",
  "return": {
    "entities": [
      0
    ],
    "relations": [
      0
    ]
  },
  "entities": [
    {
      "type": "airports",
      "constraints": [
        {
          "attribute": "city",
          "value": "New York",
          "dataType": "text",
          "matchType": "exact"
        }
      ]
    }
  ],
  "relations": [
    {
      "type": "flights",
      "depth": {
        "min": 1,
        "max": 1
      },
      "entityFrom": 0,
      "entityTo": -1,
      "constraints": [
        {
          "attribute": "Month",
          "value": "1",
          "dataType": "number",
          "matchType": "exact"
        }
      ]
    }
  ],
  "limit": 1000,
  "modifiers": [
    {
      "type": "COUNT",
      "selectedType": "entity",
      "id": 0,
      "attributeIndex": -1
    }
  ]
}
```

### Service Output

#### Cached Query Result Retrieval

```javascript
{
  "type": "query_result",
  "value": {
    "edges": [
      {
        "_from": "airports/JFK",
        "_id": "flights/286552",
        "_key": "286552",
        "_rev": "_cLqg0be--U",
        "_to": "airports/SFO",
        "attributes": {
          "ArrTime": 2332,
          "ArrTimeUTC": "2008-01-16T07:32:00.000Z",
          "Day": 15,
          "DayOfWeek": 2,
          "DepTime": 2006,
          "DepTimeUTC": "2008-01-16T01:06:00.000Z",
          "Distance": 2586,
          "FlightNum": 649,
          "Month": 1,
          "TailNum": "N597JB",
          "UniqueCarrier": "B6",
          "Year": 2008
        }
      }
    ],
    "nodes": [
      {
        "_id": "airports/SFO",
        "_key": "SFO",
        "_rev": "_cLp3eL6-_G",
        "attributes": {
          "city": "San Francisco",
          "country": "USA",
          "lat": 37.61900194,
          "long": -122.3748433,
          "name": "San Francisco International",
          "state": "CA",
          "vip": true
        }
      }
    ]
  }
}
```

Otherwise the output is simply the input message forwarded to a specific query executor.
