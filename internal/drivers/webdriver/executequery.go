/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package webdriver

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"query-orchestrator/internal/entity"

	"git.science.uu.nl/graphpolaris/go-common/microservice"
	"github.com/rs/xid"
	"github.com/rs/zerolog/log"
)

/*
Executes a query handler and returns a http handler

	authService: auth.UseCase, the usecase for the authentication service
	produceService: produce.UseCase, the usecase for the producer service
	Return: http.Handler, returns an http handler
*/
func (l *Listener) executeQueryHandler(w http.ResponseWriter, r *http.Request) {
	log.
		Trace().
		Str("queryRequestHandler", "loading headers").
		Msg("processing query request")

	// Generate a unique ID for this query
	queryID := xid.New().String()

	// Create a response containing the queryID
	response := entity.QueryID{
		QueryID: queryID,
	}

	// Write the encoded result to the http writer
	json.NewEncoder(w).Encode(response)

	// Publish a message into the query queue
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Error().Msgf("error reading body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Grab the databaseName from the request
	var temp map[string]interface{}
	json.Unmarshal(body, &temp)
	log.Debug().Msgf("temp: %v", temp)

	databaseName, ok := temp["databaseName"].(string)
	if !ok {
		log.Error().Msgf("error reading databaseName: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	sessionData, err := microservice.ContextToSessionData(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Trace().Str("Userid", sessionData.UserID).Str("Sessionid", sessionData.SessionID).Msg("found headers")

	l.produceService.ProduceQueryExecutionMessage(&body, &sessionData.SessionID, &sessionData.UserID, &databaseName, &queryID)
}
