/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package webdriver

import (
	"encoding/json"
	"net/http"
	"query-orchestrator/internal/entity"

	"git.science.uu.nl/graphpolaris/go-common/microservice"
)

/*
Retrieves the cached query

	authService: auth.UseCase, the authentication service
	retrieveService: retrievequery.UseCase, the retrieval service
	producerService: produce.UseCase, the producer service
	Return: http.Handler, returns an http handler
*/
func (l *Listener) retrieveCachedQuery(w http.ResponseWriter, r *http.Request) {

	var cachedQueryRequest entity.RetrieveCachedQuery
	err := json.NewDecoder(r.Body).Decode(&cachedQueryRequest)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	sessionData, err := microservice.ContextToSessionData(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	queryResult, err := l.retrieveService.RetrieveCachedQuery(&sessionData.UserID, &cachedQueryRequest.QueryID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	l.produceService.ProduceCachedQuery(queryResult, &sessionData.SessionID)

	w.WriteHeader(http.StatusOK)
}
