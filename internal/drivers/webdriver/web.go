/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package webdriver

import (
	"query-orchestrator/internal/usecases/produce"
	"query-orchestrator/internal/usecases/retrievequery"

	"git.science.uu.nl/graphpolaris/go-common/microservice"
)

/*
A Listener is a concrete implementation for a web listener
*/
type Listener struct {
	api             microservice.API
	produceService  *produce.Service
	retrieveService *retrievequery.Service
}

/*
CreateListener creates a web listener

	Return: *Listener, returns a web listener
*/
func CreateListener(produceService *produce.Service, retrieveService *retrievequery.Service) *Listener {
	return &Listener{
		api:             microservice.New(),
		produceService:  produceService,
		retrieveService: retrieveService,
	}
}

/*
Start starts the web listener on port 3000
*/
func (l *Listener) Start() {
	r := l.api.Routes()
	r.Post("/execute", l.executeQueryHandler)
	r.Post("/retrieve-cached", l.retrieveCachedQuery)
	l.api.Start(3003, r)
}
