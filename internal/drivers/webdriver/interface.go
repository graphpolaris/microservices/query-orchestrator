/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package webdriver

import (
	"query-orchestrator/internal/usecases/produce"
	"query-orchestrator/internal/usecases/retrievequery"
)

/*
A ListenerInterface models what a web listener should do
*/
type ListenerInterface interface {
	Start()
	SetupHandlers(produceService produce.UseCase, retrieveService retrievequery.UseCase)
}
