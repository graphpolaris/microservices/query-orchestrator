/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package produce

import (
	"log"

	"git.science.uu.nl/graphpolaris/go-common/drivers/databaserpcdriver"
)

/*
ProduceQueryExecutionMessage publishes a query execution request

	query: *[]byte, the query being executed
	sessionID: *string, the ID of the session
	userID: *string, the ID of the user
	databaseName: *string, the name of the database
	queryID: *string, the ID of the query
*/
func (s *Service) ProduceQueryExecutionMessage(query *[]byte, sessionID *string, userID *string, databaseName *string, queryID *string) {
	// Send the message to the correct queue
	// Request the database type from the user management service
	databaseType, err := databaserpcdriver.GetDatabaseType(userID, databaseName)
	if err != nil {
		return
	}

	headers := make(map[string]interface{})
	headers["sessionID"] = *sessionID
	headers["userID"] = *userID
	headers["queryID"] = *queryID

	switch *databaseType {
	case "arangodb":
		log.Println("Publishing to arangodb queue")
		s.producer.PublishMessageJsonHeaders(query, "arangodb-query-request", &headers)
	case "neo4j":
		log.Println("Publishing to neo4j queue")
		s.producer.PublishMessageJsonHeaders(query, "neo4j-query-request", &headers)
	default:
		log.Println("No valid database type found")
		return
	}

}
