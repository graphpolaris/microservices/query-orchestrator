/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package produce

import (
	"context"
	"fmt"

	"git.science.uu.nl/graphpolaris/keyvaluestore"
	"github.com/rs/zerolog/log"
)

/*
ProduceCachedQuery produces the cached query result

	queryResult: *[]byte, the query result
	sessionID: *string, the ID of the session
*/
func (s *Service) ProduceCachedQuery(queryResult *[]byte, sessionID *string) {
	// Use the sessionID to query the key value store to get the queue we need to send this message to
	clientQueueID, err := s.keyValueStore.Get(context.Background(), fmt.Sprintf("routing %s", *sessionID), keyvaluestore.String)
	if err != nil || clientQueueID == nil {
		return
	}

	log.Info().Msg(fmt.Sprintf("Found client queue %s for session %s", clientQueueID, *sessionID))

	headers := make(map[string]interface{})
	headers["sessionID"] = *sessionID
	headers["type"] = "queryResult"
	s.producer.PublishMessageJsonHeaders(queryResult, clientQueueID.(string), &headers)
}
