/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package produce

/*
UseCase is an interface describing the produce usecases
*/
type UseCase interface {
	ProduceQueryExecutionMessage(query *[]byte, sessionID *string, userID *string, databaseName *string, queryID *string)
	ProduceCachedQuery(queryResult *[]byte, sessionID *string)
}
