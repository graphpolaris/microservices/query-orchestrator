/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package retrievequery

import (
	"git.science.uu.nl/graphpolaris/go-common/microservice"
	"git.science.uu.nl/graphpolaris/keyvaluestore"
)

/*
A Service implements the retrieve query usecase
*/
type Service struct {
	api           microservice.API
	keyValueStore keyvaluestore.Interface
}

/*
NewService creates a new query retrieval service

	objectStore: objectostore.Interface, the object store for the new service
	Return: the new service
*/
func NewService(keyValueStore keyvaluestore.Interface) *Service {
	return &Service{
		api:           microservice.New(),
		keyValueStore: keyValueStore,
	}
}
