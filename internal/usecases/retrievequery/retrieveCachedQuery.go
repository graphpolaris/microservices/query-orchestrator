/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package retrievequery

import (
	"context"
	"fmt"
	"time"

	"git.science.uu.nl/graphpolaris/keyvaluestore"
)

/*
RetrieveCachedQuery retrieves a cached query from the object store

	userID: *string, the ID of the user
	queryID: *string, the ID of the query
	Return: (*[]byte, error), returns the cached query and an error if there is one
*/
func (s *Service) RetrieveCachedQuery(userID *string, queryID *string) (*[]byte, error) {
	getQueryContext, getQueryCancel := context.WithTimeout(context.Background(), time.Second*20)
	defer getQueryCancel()

	// Get the cached query result from the object store
	// Query results are stored under the object name sessionID-queryID
	byteArray, err := s.keyValueStore.Get(getQueryContext, fmt.Sprintf("cached-queries %s-%s", *userID, *queryID), keyvaluestore.Bytes)
	if err != nil {
		return nil, err
	}

	var emptyByteArray []byte = byteArray.([]byte)
	return &emptyByteArray, err
}
