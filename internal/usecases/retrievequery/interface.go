/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package retrievequery

/*
UseCase describes method a query retriever service should implement
*/
type UseCase interface {
	RetrieveCachedQuery(userID *string, queryID *string) (*[]byte, error)
}
