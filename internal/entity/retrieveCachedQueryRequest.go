/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

import (
	"encoding/json"
	"errors"
)

/*
RetrieveCachedQuery models an incoming cached query request
*/
type RetrieveCachedQuery struct {
	QueryID string `json:"queryID"`
}

/*
UnmarshalJSON unmarshals data into the RetrieveCachedQuery struct

	data: []byte, the data to be unmarshalled
	Return: error, returns a potential error
*/
func (rqr *RetrieveCachedQuery) UnmarshalJSON(data []byte) error {
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}

	var valid bool = false
	if rqr.QueryID, valid = v["queryID"].(string); !valid {

		return errors.New("missing field 'queryID' or not type 'string'")
	}

	return nil
}
