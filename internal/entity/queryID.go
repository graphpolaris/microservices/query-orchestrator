/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package entity

/*
QueryID describes the message sent to the frontend when a new query is executed
*/
type QueryID struct {
	QueryID string `json:"queryID"`
}
