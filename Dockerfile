FROM golang:1.21 as build

ARG TARGETARCH
ARG TARGETOS

WORKDIR /app
COPY . .

# Remove all lines starting with `replace` in go.mod
RUN sed -i '/^replace/d' go.mod

RUN go mod download

RUN GOARCH=${TARGETARCH} GOOS=${TARGETOS} CGO_ENABLED=0 go build -o ./main ./cmd/query-orchestrator/

FROM alpine:3 as certs

RUN apk update && \
    apk --no-cache add ca-certificates

FROM alpine:3

COPY --from=certs /etc/ssl/certs /etc/ssl/certs

WORKDIR /app
COPY --from=build /app/main ./

CMD ["./main"]
